FROM nginx

ARG html=build
ARG conf=nginx.conf

COPY ${conf} /etc/nginx/conf.d/default.conf
COPY ${html} /usr/share/nginx/html/