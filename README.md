# EA Coding Test
Author: Juan Carlos Fernández Conde <fconde.j@gmail.com>

Using React 16.9.0 and Redux 4.0.4

[See demo](http://35.222.152.217/)

### Requirements
- **NPM** (recommended version: 8.15.0+).

### Install
Clone this repository, then cd into the cloned repository's dir (ea-test).
```
npm install
```

### Run
Once installed, just run:
```
npm start
```

### App Description
This repository contains a react-redux application which showcase a solution for the suggested coding test via e-mail.

The application has the following features: 
- **/!\ Mockups**: since there is no backend, the endpoints now point to .json files. In addition, the mockData function and the augmentedBody in the sagas perform a little logic to intercept and generate correct IDs for the tree to work correctly.  
- **LocCMS speech tree**:
  - The first level of the tree (features) is first loaded.  Nested levels of the tree are loaded when its parent node is expanded for the first time.
  - If the load of children nodes completes successfully, the list is saved in the Redux store. Expanding and collapsing the same node will only toggle its load once.
  - The empty view when no features are found can be tested by blocking the list.json request in Google Chrome.   
  - Reusable component LocTree that renders a tree given a set of props. 
    - It receives a sorted array of types that indicates, for a node of a given type, which type its child or parent must have.
    - It supports easily adding or removing new levels in the tree hierarchy, just by editing the nodeTypeOrder const in src/app/nodeTypes.js.
    - It supports a tool renderer, that will be called for each node with the node data to render additional buttons or components.
  - Elements of type Sentence show an "Add" tool that shows a prompt and performs a POST request to the server.
  - Elements of type Event show a "Remove" tool that performs a DELETE request to the server.
- **Element detail**:
  - This view can be displayed by clicking on the code or name of a tree node.
  - It loads its data using the selected node id (retrieved as a URL path param) performing a GET request.
  - It is displayed as a modal view with a mask: clicking outside the detail will hide the component and navigate back to the master (tree).
- Using **redux-saga** for executing asynchronous logic that complies with redux, **react-router-dom** for routing and **axios** library for HTTP Requests.
- App logic split into 2 containers: locCMS and locCMS/detail.
- Components under /app handle business logic: performing requests, transforming the state, routing... but not representional logic.
- Components under src/widgets do not handle business logic and never import any file from outside said folder, appart from external dependencies..
- Components under src/widgets/ui should be re-usable in a straight-forward way.
- Routing example is included:
  - /locCMS - shows the LocCMS speech tree.
  - /locCMS:id - shows the detail on top of the speech tree.
  - \<Every other URL\> - Redirect to /locCMS (configurable).
    - When deployed on Nginx, reloading in /locCMS might not work. Navigating to / should solve this.  
- Very basic styles included as CSS files adjacent to views (includes Roboto font, free icon font with 9 icons). 
- Includes Dockerfile for building a nginx image serving the contents of the build/ folder.
- Includes example kubernetes yaml files for deploying the app on a cluster.
- I have run out of time and have not been able to include tests, but **it is extremely important** in every web app to have several different tests (unit, integration, UI...) for each and every feature, library and Component that is coded, with extensive coverage.

### TO-DO:
The following things have not been included because of the scope of the test, but should always be taken into account:
- I18n and L10n: translating literals, dates, currencies, data and every other possible localizable element.
- HTTP Library or wrapper around an existing library: move URIs as away from the code, and define them as configuration.
- Thorough testing (please see the last point in App description).
- User and session management.
- Permissions/roles management.
- Asking the user for confirmation before performing sensitive operations (deleting a tree node, in this case). 