import React from 'react';
import {Provider as ReduxProvider} from 'react-redux';
import getAppStore from './store';
import routes from './routes';
import Viewport from './app/Viewport';

const store = getAppStore();

function App() {
    return (
        <ReduxProvider store={store}>
            <Viewport routes={routes}/>
        </ReduxProvider>
    );
}

export default App;
