import React, {useCallback} from 'react';
import './Tool.css';
import PropTypes from 'prop-types';

const noop = function () {};
const Tool = ({data, type = '', onClick = noop} = {}) => {
    const onToolClick = useCallback(() => onClick(data), [onClick, data]);

    return (
        <div className={`tree-node-tool icon icon-${type} type-${type || 'default'}`} onClick={onToolClick}>
            <span className="tree-node-tool__icon"/>
        </div>
    );
};

Tool.propTypes = {
    data   : PropTypes.object,
    type   : PropTypes.string,
    onClick: PropTypes.func
};

export default Tool;