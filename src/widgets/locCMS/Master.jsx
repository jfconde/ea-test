import React, {useCallback} from 'react';
import LocTree from '../ui/LocTree';
import Tool from './Tool';
import './Master.css';
import PropTypes from 'prop-types';

const Master = (props = {}) => {
    const {
              nodeTypeCodes = {},
              nodeTypeOrder,
              data          = {},
              loadNodeList,
              expandNode,
              collapseNode,
              onNodeClick,
              addNode,
              removeNode,
              loadedParentNodeIds,
              loadingNodeIds,
              expandedNodeIds
          } = props;

    const toolRenderer = useCallback((node) => {
        switch (node.type) {
            case nodeTypeCodes.event:
                return [<Tool key="add" type="add" data={node} onClick={addNode}/>];
            case nodeTypeCodes.sentence:
                return [<Tool key="remove" type="remove" data={node} onClick={removeNode}/>];
            default:
                return [];
        }
    }, [nodeTypeCodes, addNode, removeNode]);

    return (
        <div className="loccms-master">
            <h1 className="loccms-master__title">LocCMS:</h1>
            <LocTree
                data={data}
                loadNodeChildren={loadNodeList}
                nodeTypeOrder={nodeTypeOrder}
                onExpand={expandNode}
                onCollapse={collapseNode}
                loadedParentNodeIds={loadedParentNodeIds}
                loadingNodeIds={loadingNodeIds}
                expandedNodeIds={expandedNodeIds}
                toolRenderer={toolRenderer}
                onNodeClick={onNodeClick}
            />
        </div>

    );
};

Master.propTypes = {
    data               : PropTypes.object.isRequired,
    nodeTypeCodes      : PropTypes.object.isRequired,
    nodeTypeOrder      : PropTypes.array.isRequired,
    loadedParentNodeIds: PropTypes.array.isRequired,
    loadingNodeIds     : PropTypes.array.isRequired,
    expandedNodeIds    : PropTypes.array.isRequired,
    loadNodeList       : PropTypes.func.isRequired,
    onNodeClick        : PropTypes.func.isRequired,
    expandNode         : PropTypes.func.isRequired,
    collapseNode       : PropTypes.func.isRequired,
    addNode            : PropTypes.func.isRequired,
    removeNode         : PropTypes.func.isRequired
};

export default Master;