import React, {useEffect, useRef} from 'react';
import EntityDetail from '../ui/EntityDetail';
import './Detail.css';
import PropTypes from 'prop-types';

const entityFields = [
    {name: 'code', text: 'Code', description: 'Code associated to this entity.'},
    {name: 'name', text: 'Name', description: 'A descriptive name associated to this entity.'},
    {name: 'type', text: 'Type', description: 'The type of this entity determines its behaviour and hierarchy.'}
];

const Detail = (props = {}) => {
    const {data, loading, loadData, onOuterAreaClick, match: {params: {id}}} = props;
    const formData = data || {};
    const detailEl = useRef(null);

    useEffect(() => {
        id && loadData(id);
    }, [loadData, id]);

    // Detect clicks outside the detail component.
    useEffect(() => {
        const clickHandler = (event) => {
            const el = detailEl.current;
            // If the current detail does not contain the target of the click event...
            if (!el.contains(event.target)) {
                onOuterAreaClick && onOuterAreaClick();
                event.preventDefault();
            }
        };

        document.addEventListener('click', clickHandler, false);
        return () => document.removeEventListener('click', clickHandler, false);
    }, [onOuterAreaClick]);

    return (
        <div className="loccms-detail">
            <div className="loccms-detail__mask"></div>
            <div ref={detailEl} className="loccms-detail__body">
                <EntityDetail title={formData.name} loading={loading}>
                    {entityFields.map(field => (
                        <div className="node-field" key={field.name}>
                            <span className="node-field__label">{field.text}:</span>
                            <span className="node-field__value">{formData[field.name]}</span>
                            <span className="node-field__description">{field.description}</span>
                        </div>
                    ))}
                </EntityDetail>
            </div>
        </div>

    );
};

Detail.propTypes = {
    data            : PropTypes.object,
    loading         : PropTypes.bool,
    loadData        : PropTypes.func,
    onOuterAreaClick: PropTypes.func
    // + withRouter props (match)
};

export default Detail;