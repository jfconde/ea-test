import React, {useCallback} from 'react';
import './TreeNode.css';
import PropTypes from 'prop-types';

const noop = function () {};

const TOOL_NONE = 0;
const TOOL_EXPANDER = 1;
const TOOL_SPINNER = 2;

const TreeNode = (props = {}) => {
    const {data = {}, expanded = false, loading = false, onExpand = noop, onCollapse = noop, onInfoClick = noop, tools = [], children} = props;
    const {leaf, code, name} = data;

    const onExpanderClick = useCallback((event) => {
        const expanderFn = expanded ? onCollapse : onExpand;
        event.stopPropagation();
        expanderFn(data, event);
    }, [onCollapse, onExpand, expanded, data]);

    const onInformationClick = useCallback((event) => onInfoClick(data, event), [onInfoClick, data]);
    const visibleTool = loading ? TOOL_SPINNER : leaf ? TOOL_NONE : TOOL_EXPANDER;

    return (
        <div className="tree-node">
            <div className="tree-node__ct">
                {visibleTool === TOOL_NONE && <span className="tree-tool tool-empty"></span>}
                {visibleTool === TOOL_SPINNER && <span className="tree-tool icon icon-spinner tool-spinner"/>}
                {visibleTool === TOOL_EXPANDER && (
                    <span
                        onClick={onExpanderClick}
                        className={`tree-tool tool-expander icon icon-${expanded ? 'collapse' : 'expand'}`}
                    />
                )}
                <span className="tree-node__info" onClick={onInformationClick}>
                    <span className="tree-node__info__code">{code}</span>
                    <span className="tree-node__info__name">{name}</span>
                </span>
                <div className="tree-node__tools">{tools || []}</div>
            </div>
            {children}
        </div>
    );
};

TreeNode.propTypes = {
    data       : PropTypes.object.isRequired,
    expanded   : PropTypes.bool,
    loading    : PropTypes.bool,
    onCollapse : PropTypes.func,
    onExpand   : PropTypes.func,
    onInfoClick: PropTypes.func,
    tools      : PropTypes.array
};

export default TreeNode;