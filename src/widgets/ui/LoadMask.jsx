import React from 'react';
import './LoadMask.css';
import PropTypes from 'prop-types';

const LoadMask = (props = {}) => {
    const {text = 'Loading...'} = props;

    return (<div className="load-mask">
        <span className="load-mask__icon icon icon-spinner"></span>
        <div className="load-mask__text">{text}</div>
    </div>);
};

LoadMask.propTypes = {
    text: PropTypes.string
};

export default LoadMask;