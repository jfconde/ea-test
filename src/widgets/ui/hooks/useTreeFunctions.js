import {useCallback, useEffect, useMemo} from 'react';

function useTreeFunctions(options = {}) {
    const {
              data,
              loadNodeChildren,
              loadedParentNodeIds,
              nodeTypeOrder,
              onCollapse,
              onExpand
          } = options;

    // Load the first level of the tree on mount
    useEffect(() => {
            if (!(Object.keys(data || {}).length)) {
                loadNodeChildren();
            }
        }, [data, loadNodeChildren]
    );
    // On node collapse, just call the onCollapse prop, if passed.
    const onNodeCollapse = useCallback((node) => onCollapse && onCollapse(node), [onCollapse]);
    // On node expand, call the on onExpand prop, if passed, and loadNodeChildren if the node children have not been loaded.
    const onNodeExpand = useCallback((node) => {
        !loadedParentNodeIds.includes(node.id) && !node.leaf && loadNodeChildren(node);
        onExpand && onExpand(node);
    }, [onExpand, loadedParentNodeIds, loadNodeChildren]);

    const hasContent = useMemo(() => data && data[nodeTypeOrder[0]] && data[nodeTypeOrder[0]].length, [data, nodeTypeOrder]);

    return [onNodeCollapse, onNodeExpand, hasContent];
}

export default useTreeFunctions;