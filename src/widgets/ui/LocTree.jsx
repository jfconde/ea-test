import React from 'react';
import TreeNode from './TreeNode';
import './LocTree.css';
import useTreeFunctions from './hooks/useTreeFunctions';
import PropTypes from 'prop-types';

/**
 * Recursive function that and generates each tree levels.
 * It iterates over the provided nodeTypeOrder array to find in the data object/cache
 * the necessary records to render, based on their parentId.
 */
const renderChildNodes = (config = {}) => {
    const {
              data            = {},
              depth           = 0,
              expandedNodeIds = [],
              loadingNodeIds  = [],
              nodeTypeOrder   = [],
              onNodeCollapse,
              onNodeExpand,
              onNodeClick,
              parentId,
              toolRenderer
          } = config;

    return (data[nodeTypeOrder[depth]] || [])
        .filter(node => !parentId || node.parentId === parentId)
        .map(node => {
            const {leaf, id} = node;
            const expanded = expandedNodeIds.includes(id);
            const isNodeLoading = loadingNodeIds.includes(id);
            const mustRecurse = !leaf && expandedNodeIds.includes(id);

            return <TreeNode
                key={id}
                data={node}
                expanded={expanded}
                loading={isNodeLoading}
                tools={toolRenderer ? toolRenderer(node) : []}
                onExpand={onNodeExpand}
                onCollapse={onNodeCollapse}
                onInfoClick={onNodeClick}
            >
                {mustRecurse && renderChildNodes({
                    ...config,
                    parentId: id,
                    depth   : depth + 1
                })}
            </TreeNode>;
        });
};

const LocTree = (props = {}) => {
    const {
              data = {},
              nodeTypeOrder,
              onNodeClick,
              loadingNodeIds,
              expandedNodeIds,
              toolRenderer
          } = props;

    const [onNodeCollapse, onNodeExpand, hasContent] = useTreeFunctions(props);

    return (
        <div className={`loc-tree ${hasContent ? '' : 'empty'}`}>
            {hasContent ?
             renderChildNodes({
                 data,
                 expandedNodeIds,
                 loadingNodeIds,
                 nodeTypeOrder,
                 onNodeExpand,
                 onNodeCollapse,
                 onNodeClick,
                 toolRenderer
             }) : (
                 <section className="empty-view">
                     <i className="empty-view__icon icon-event"/>
                     <span
                         className="empty-view__text">The current LocCMS tree does not contain any features yet.</span>
                 </section>
             )}
        </div>
    );
};

LocTree.propTypes = {
    data               : PropTypes.object.isRequired,
    nodeTypeOrder      : PropTypes.array.isRequired,
    expandedNodeIds    : PropTypes.array.isRequired,
    loadedParentNodeIds: PropTypes.array.isRequired,
    loadingNodeIds     : PropTypes.array.isRequired,
    loadNodeChildren   : PropTypes.func.isRequired,
    onNodeClick        : PropTypes.func,
    toolRenderer       : PropTypes.func,
    onCollapse         : PropTypes.func,
    onExpand           : PropTypes.func
};

export default LocTree;