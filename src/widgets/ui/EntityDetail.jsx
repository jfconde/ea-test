import React from 'react';
import LoadMask from './LoadMask';
import './EntityDetail.css';
import PropTypes from 'prop-types';

const Header = (props = {}) => {
    const {title} = props;
    return (
        <header className="entity-header">
            <h2 className="entity-header__title">{title}</h2>
        </header>
    );
};

Header.propTypes = {
    title: PropTypes.string
};

const EntityDetail = (props = {}) => {
    const {title, children, loading} = props;

    return (
        <div className="entity-detail">
            {loading ?
             <LoadMask/> :
             [
                 <Header className="entity-detail__header" title={title} key="header"/>,
                 <div className="entity-detail__body" key="body">
                     {children}
                 </div>
             ]
            }
        </div>
    );
};

EntityDetail.propTypes = {
    title  : PropTypes.string,
    loading: PropTypes.bool.isRequired
};

export default EntityDetail;