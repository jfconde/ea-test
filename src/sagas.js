import locCMSSagas from './app/locCMS/sagas';
import {all} from 'redux-saga/effects';

export default function * () {
    yield all([
        ...locCMSSagas
    ]);
}