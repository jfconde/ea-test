/**
 * This is the only mapping of entity names to the "type" attributes of the
 * data we are expecting in our entity models (features, contexts, events,
 * sentences).
 *
 * Should the type of any entity change (this should not happen), it would
 * just be enough to adjust the mapping in this object.
 */
export const nodeTypeCodes = {
    feature : 'feature',
    context : 'context',
    event   : 'event',
    sentence: 'sentence'
};

/**
 * This is the only place where we define the hierarchy in the tree.
 *
 * Should the necessity arise to introduce a new, intermediate
 * entity, it would be enough to define its code and add it in the
 * appropriate place in this array.
 */
export const nodeTypeOrder = [
    nodeTypeCodes.feature,
    nodeTypeCodes.context,
    nodeTypeCodes.event,
    nodeTypeCodes.sentence
];

export const getParentEntityType = (type) => nodeTypeOrder[nodeTypeOrder.indexOf(type) - 1];
export const getChildEntityType = (type) => nodeTypeOrder[nodeTypeOrder.indexOf(type) + 1];
