import {nodeTypeOrder, getChildEntityType} from '../nodeTypes';
const prefixActionType = type => `APP/LOCCMS/${type}`;

export const actions = {
    LOAD_NODE_LIST  : prefixActionType('LOAD_NODE_LIST'),
    EXPAND_NODE     : prefixActionType('EXPAND_NODE'),
    COLLAPSE_NODE   : prefixActionType('COLLAPSE_NODE'),
    ADD_NODE        : prefixActionType('ADD_NODE'),
    REMOVE_NODE     : prefixActionType('REMOVE_NODE')
};
// Action creators
export const loadNodeList = ({id, type = nodeTypeOrder[0]} = {}) => ({
    type   : actions.LOAD_NODE_LIST,
    payload: {id, type: id ? getChildEntityType(type) : nodeTypeOrder[0]}
});

export const expandNode = (id) => ({
    type   : actions.EXPAND_NODE,
    payload: {id}
});

export const collapseNode = (id) => ({
    type   : actions.COLLAPSE_NODE,
    payload: {id}
});

export const addNode = ({parentId, parentType, name}) => ({
    type   : actions.ADD_NODE,
    payload: {parentId, parentType, name}
});

export const removeNode = ({id, type}) => ({
    type   : actions.REMOVE_NODE,
    payload: {id, type}
});