import {actions} from './actions';
import {successAction, failureAction, uniqueArray, withChildReducers} from '../utils';
import {getChildEntityType, getParentEntityType} from '../nodeTypes';
import detail from './detail/reducer';

const initialState = {
    loading            : false,
    data               : {},
    loadedParentNodeIds: [],
    loadingNodeIds     : [],
    expandedNodeIds    : []
};

/**
 * Warning: usable only when siblings of the removed node have been loaded. It works correctly now because:
 *  - When we delete a node we always have its parent expanded, and therefore the rest of possible siblings.
 *  - When we add a node that node will always be present so, regardless of any other possible unloaded siblings,
 *    the leaf attribute will always be false in this case.
 *
 * Should the UI/UX change, a more precise control of leaf status must be built (probably requiring more server info).
 *
 * Generate a new snapshot of the tree data that, given a node id, has its leaf attribute automatically calculated
 * from the count of child nodes that it has (explaining the warning).
 * @param {Object} data Current snapshot of the data/cache.
 * @param {Number} id Id of the parentNode to check.
 * @param {String} type Type of the parentNode to check.
 * @param {string} childType Type of the children.
 * @returns {Object} the new snapshot of the tree, sorted by id.
 */
const checkLeafStatus = (data, id, type, childType) => {
    const existingNode = data[type].find(node => node.id === id);
    if (!existingNode) {
        console.warn(`Tried to check leaf status of node with id ${id} and type ${type} but it was not found in the state.`);
        return data;
    }
    // Calculate the new child count by filtering for the nodes of the child type with parentId === removed node id.
    const newChildCount = data[childType].filter(node => node.parentId === id).length;
    // Now that we have calculated the new child count, we can set the leaf value.
    return {
        ...data,
        [type]: [
            // The list without the parent node.
            ...(data[type].filter(node => node.id !== id)),
            // The new parent node with its leaf calculated.
            {...existingNode, leaf: !newChildCount}
        ].sort((a, b) => a.id - b.id)
    };
};

/**
 * Create a new snapshot of the state/cache adding the loaded list of elements, given their type.
 * If an element is loaded and it was already present in the state/cache, the loaded (most recent) version is kept.
 * Also, update the leaf status of its parentNode if it is necessary.
 *
 * @param {Object} currentData Current snapshot of the data/cache.
 * @param {String} type Type of the loaded elements.
 * @param {Number} parentId Id of the node to append this nodes into.
 * @param {Array} listData The array with the loaded elements
 * @returns {*} the new snapshot of the tree, sorted by id.
 */
const addNewListData = (data = {}, type, parentId, listData = []) => {
    if (parentId) {
        // Loading a specific node (not the top-most level).
        const currentNodeTypeList = data[type] || [];
        const newNodeIds = listData.map(node => node.id);
        // In this case, we need to add the new node data to the appropriate list in currentData (currentNodeTypeList).
        // We will merge the existing value with listData, but if the same node (same id) exists in
        // both the new data and the old one, the new information has preference.
        const newData = {
            ...data,
            // Depending on the type of nodes loaded, we will modify the feature array, or the context array, etc.
            [type]: [
                ...currentNodeTypeList.filter(node => !newNodeIds.includes(node.id)), // The previous list without the equivalent of the new nodes (same id).
                ...listData // New nodes
            ]
        };
        return checkLeafStatus(newData, parentId, getParentEntityType(type), type);
    } else {
        // Since we are loading the top-most level of the tree, which will only happen on re-mount, flush the state data.
        return {
            [type]: listData
        };
    }
};

/**
 * Create a new snapshot of the state/cache removing the selected element, given its type.
 * Also, update the leaf status of its parentNode if it is necessary.
 *
 * @param {Object} data Current snapshot of the data/cache.
 * @param {String} type Type of the removed node.
 * @param {Number} id Id of the removed node.
 * @returns {*} the new snapshot of the tree, sorted by id.
 */
const removeNodeFromTree = (data, id, type) => {
    // We must first find the existing instance.
    const existingNode = data[type].find(node => node.id === id);
    if (!existingNode) {
        console.warn(`Tried to remove node with id ${id} and type ${type} but it was not found in the state.`);
        return data;
    }
    // We remove the node from the list of its type.
    let resData = {...data, [type]: data[type].filter(node => node.id !== id)};
    // Make sure that the parent of the removed node does not have its leaf status wrong (it could now be a leaf).
    resData = checkLeafStatus(resData, existingNode.parentId, getParentEntityType(type), type);

    // And now we iterate through the chain of nodeTypes that this node is ancestor of.
    // In each iteration, we delete the nodes whose parentId matches the id of the deleted nodes
    // in the previous iteration (we remove orphaned children from the previous iteration).
    let currentNodeType = getChildEntityType(type);
    let removedParents = [id];

    while (currentNodeType && removedParents.length) {
        const removedParentsCopy = [...removedParents];
        const childIdsToRemove = resData[currentNodeType].filter(node => removedParentsCopy.includes(node.parentId)).map(node => node.id);
        resData = {
            ...resData, [currentNodeType]: resData[currentNodeType].filter(node => !childIdsToRemove.includes(node.id))
        };
        // Prepare the following iteration.
        removedParents = childIdsToRemove;
        currentNodeType = getChildEntityType(currentNodeType);
    }
    return resData;
};

const removeLoadingId = (state, action, payload) => ({
    ...state,
    loading       : payload.id ? state.loading : false,
    loadingNodeIds: state.loadingNodeIds.filter(id => id !== payload.id)
});

const handlers = {
    /**
     * When firing the load of a list of nodes:
     *   - Add the loading attribute if there is no id (top-most tree level uses this loadMask, there is no parent node id).
     *   - Wipe all data from the state/cache if there is no id (see the line above).
     *   - Add the loaded node id to the list of loading node ids, if there is an id (see 2 lines above).
     */
    [actions.LOAD_NODE_LIST]               : (state, action, payload) => ({
        ...state,
        loading       : payload.id ? state.loading : true,
        data          : payload.id ? state.data : initialState.data, // When loading the first level of the tree, flush all the data.
        loadingNodeIds: payload.id ? uniqueArray([...state.loadingNodeIds, payload.id]) : state.loadingNodeIds
    }),
    /**
     * When the load of a list of nodes completes:
     *   - Remove the loading attribute if there is no id (top-most tree level uses this loadMask, there is no parent node id).
     *   - Generate a new snapshot of the data that contains the new entities at their right place, sorted by id.
     *   - Remove the loaded node id from the list of loading node ids, if there is an id (see 2 lines above).
     *   - Add the parent node id to the list of loaded parent node ids, so that the next time the user expands we use the cache data.
     */
    [successAction(actions.LOAD_NODE_LIST)]: (state, action, payload) => ({
        ...state,
        loading            : payload.id ? state.loading : false,
        data               : addNewListData(state.data, payload.type, payload.id, payload.data),
        loadingNodeIds     : state.loadingNodeIds.filter(id => id !== payload.id),
        loadedParentNodeIds: payload.id ? uniqueArray([...state.loadedParentNodeIds, payload.id]) : state.loadedParentNodeIds
    }),
    /**
     * To expand a node:
     *   - Add the node to the list of expanded node ids.
     */
    [actions.EXPAND_NODE]                  : (state, action, payload) => ({
        ...state,
        expandedNodeIds: uniqueArray([...state.expandedNodeIds, payload.id])
    }),
    /**
     * To collapse a node:
     *   - Remove the node from the list of expanded node ids.
     */
    [actions.COLLAPSE_NODE]                : (state, action, payload) => ({
        ...state,
        expandedNodeIds: state.expandedNodeIds.filter(id => id !== payload.id)
    }),
    /**
     * When firing the creation of a new node:
     *   - Add its parent node id to the list of loading node ids.
     */
    [actions.ADD_NODE]                     : (state, action, payload) => ({
        ...state,
        loadingNodeIds: uniqueArray([...state.loadingNodeIds, payload.parentId])
    }),
    /**
     * When the creation of a new node completes:
     *   - Generate a new snapshot of the data that contains the new node at its right place, sorted by id.
     *   - Remove its parent node id from the list of loading node ids.
     */
    [successAction(actions.ADD_NODE)]      : (state, action, payload) => ({
        ...state,
        data          : addNewListData(state.data, payload.data.type, payload.parentId, [payload.data]),
        loadingNodeIds: state.loadingNodeIds.filter(id => id !== payload.parentId)
    }),
    /**
     * When firing the deletion of a node:
     *   - Add its parent node id to the list of loading node ids.
     */
    [actions.REMOVE_NODE]                  : (state, action, payload) => ({
        ...state,
        loadingNodeIds: uniqueArray([...state.loadingNodeIds, payload.id])
    }),
    /**
     * When the deletion of a node completes:
     *   - Generate a new snapshot of the data that does not contain the deleted node, sorted by id.
     *   - Remove its parent node id from the list of loading node ids.
     */
    [successAction(actions.REMOVE_NODE)]   : (state, action, payload) => ({
        ...state,
        loadingNodeIds: state.loadingNodeIds.filter(id => id !== payload.id),
        data          : removeNodeFromTree(state.data, payload.id, payload.type)
    }),
    [failureAction(actions.REMOVE_NODE)]   : removeLoadingId,
    [failureAction(actions.LOAD_NODE_LIST)]: removeLoadingId
};

const reducer = (state = initialState, action = {}) => {
    return Object.keys(handlers)
    // Filter only handlers whose key matches the action type
        .filter(k => k === action.type)
        // Reduce, with the current state as initial value, invoking each handler and using its output as input to the next.
        .reduce((curState, key) => handlers[key](curState, action, (action && action.payload) || {}) || curState, state);
};

export default withChildReducers(reducer, {detail});