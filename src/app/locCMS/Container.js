import {connect} from 'react-redux';
import LocCMSMaster from '../../widgets/locCMS/Master';
import {loadNodeList, expandNode, collapseNode, addNode, removeNode} from './actions';
import {nodeTypeOrder, nodeTypeCodes} from '../nodeTypes';
import {withRouter} from 'react-router-dom';
import routes from '../../routes';

const mapStateToProps = (state = {}) => ({
    /* TODO: in order to avoid creating more "boilerplate"/intermediate components now,
     * we are passing these constant values which do not really come from the state.
     * It would be better to pass them as prop from another component or read them from
     * the server.
     */
    ...state.locCMS,
    nodeTypeOrder,
    nodeTypeCodes
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    loadNodeList: data => dispatch(loadNodeList(data)),
    removeNode  : data => dispatch(removeNode(data)),
    expandNode  : ({id}) => dispatch(expandNode(id)),
    collapseNode: ({id}) => dispatch(collapseNode(id)),
    /**
     * Add a new node.
     * I apologise for using a native JS prompt instead of a proper dialog or in-tree editor, but I am
     * really running out of time.
     * @param id The id of the parent node for the new node.
     * @param type The type of the parent node for the new node.
     */
    addNode     : ({id, type}) => dispatch(addNode({
        parentId  : id,
        parentType: type,
        name      : prompt('Please enter the name of the new sentence')
    })),
    onNodeClick : ({id}) => ownProps.history.push(routes.locCMS.detail.replace(':id', id))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LocCMSMaster));