import {connect} from 'react-redux';
import LocCMSDetail from '../../../widgets/locCMS/Detail';
import {loadData} from './actions';
import {withRouter} from 'react-router-dom';
import routes from '../../../routes';

const mapStateToProps = ({locCMS: {detail}} = {}) => ({
    ...detail
});

const mapDispatchToProps = (dispatch, {history}) => ({
    loadData        : (id) => dispatch(loadData(id)),
    onOuterAreaClick: () => history.push(routes.locCMS.root)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LocCMSDetail));