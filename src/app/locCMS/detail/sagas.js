import axios from 'axios';
import {takeLatest, put, call} from 'redux-saga/effects';
import {actions} from './actions';
import {successAction, failureAction} from '../../utils';

// TODO: Move endpoint URIs out of the saga code, and pass them via configuration.
const endpointURIs = {
    detail: '/data/LocCMS/detail.json?id={id}'
};

const loadNodeDataSaga = function* (action) {
    const {payload: {id: _id}} = action;
    const id = parseInt(_id);

    try {
        if (!id) {
            throw new Error(`Error trying to load node data for node with id: ${id}.`);
        }

        // Since we are using a dev server we simulate the behaviour of path params.
        const {data: responseData} = yield call(() => axios.get(endpointURIs.detail.replace('{id}', id)));
        yield put({
            type   : successAction(actions.LOAD_DATA),
            payload: {data: responseData.data, id}
        });
    } catch (error) {
        yield put({
            type   : failureAction(actions.LOAD_DATA),
            payload: {
                error, id
            }
        });
    }
};

export default [
    takeLatest(actions.LOAD_DATA, loadNodeDataSaga)
];