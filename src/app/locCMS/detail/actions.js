const prefixActionType = type => `APP/LOCCMS/DETAIL/${type}`;

export const actions = {
    LOAD_DATA: prefixActionType('LOAD_DATA')
};
// Action creators
export const loadData = (id) => ({
    type   : actions.LOAD_DATA,
    payload: {id}
});