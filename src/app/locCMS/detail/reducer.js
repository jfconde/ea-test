import {actions} from './actions';
import {successAction, failureAction} from '../../utils';

const initialState = {
    data   : null,
    loading: false
};

const handlers = {
    [actions.LOAD_DATA]               : (state) => ({
        ...state,
        data   : null,
        loading: true
    }),
    [successAction(actions.LOAD_DATA)]: (state, action, payload) => ({
        ...state,
        data   : payload.data,
        loading: false
    }),
    [failureAction(actions.LOAD_DATA)]: (state) => ({
        ...state,
        loading: false
    })
};

const reducer = (state = initialState, action = {}) => {
    return Object.keys(handlers)
    // Filter only handlers whose key matches the action type
        .filter(k => k === action.type)
        // Reduce, with the current state as initial value, invoking each handler and using its output as input to the next.
        .reduce((curState, key) => handlers[key](curState, action, (action && action.payload) || {}) || curState, state);
};

export default reducer;