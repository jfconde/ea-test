import React from 'react';
import {Route} from 'react-router-dom';
import routes from '../../routes';
import MasterContainer from './Container';
import DetailContainer from './detail/Container';

const Viewport = () => {
    return (
        <div className="loccms-viewport">
            <MasterContainer/>
            <Route path={routes.locCMS.detail} exact component={DetailContainer}/>
        </div>
    );
};

export default Viewport;
