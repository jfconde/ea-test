import axios from 'axios';
import {takeEvery, put, call} from 'redux-saga/effects';
import {actions} from './actions';
import {getChildEntityType} from '../nodeTypes';
import {successAction, failureAction} from '../utils';
import detailSagas from './detail/sagas';

// TODO: Remove this when an API exists. Just for mocking purposes
/**
 * Since we only have Create React App's dev server, we cannot get dynamic data
 * for this app.
 *
 * We use this function to "augment" the received data from the JSON with generated attributes
 * that simulate some logic and make the tree consistent.
 */
const mockData = (data = [], parentId = 0, type) => data.map(node => {
    const id = parentId * 10 + node.id;
    return {
        ...node,
        id,
        parentId,
        type,
        leaf: type !== 'sentence' ? node.leaf : true,
        name: `${type}`,
        code: id.toString().split('').join('.')
    };
});

// TODO: Move endpoint URIs out of the saga code, and pass them via configuration.
const endpointURIs = {
    list  : '/data/LocCMS/list.json',
    detail: '/data/LocCMS/detail.json'
};

const loadNodeListSaga = function* (action) {
    const {payload: {type, id}} = action;

    try {
        if (!type) {
            throw new Error(`Error trying to load node list with type: ${type}, parent id: ${id || 'none'}.`);
        }

        const params = {id};
        const {data: responseData} = yield call(() => axios.get(endpointURIs.list, {params}));
        yield put({
            type   : successAction(actions.LOAD_NODE_LIST),
            payload: {data: mockData(responseData.data, id, type), type, id}
        });
    } catch (error) {
        yield put({type: failureAction(actions.LOAD_NODE_LIST), payload: {error, id}});
    }
};

const addNodeSaga = function* (action) {
    const {payload: {parentType, parentId, name}} = action;

    try {
        if (!parentType || !parentId || !name) {
            throw new Error(`Error trying to create node with name "${name}", parent type: ${parentType}, parnt id: ${parentId}.`);
        }

        const body = {
            parentId,
            name,
            // TODO: for mocking purposes only. The code should be generated on the server-side or from the ordering of the elements.
            code: parentId.toString().split('').join('.') + '.' + parseInt(Math.random() * 1000 + 50).toString(), // Generate a random new code in order to, at least, render properly instead of showing empty values.
            type: getChildEntityType(parentType),
            leaf: true
        };

        // The HTTP Verb should be DELETE, but since React dev server does not support that verb, we will mock it in non-production envs.
        console.log(`INFO: Since the development server does not support POST, a GET request will be fired instead for demonstration. Request body: ${JSON.stringify(body, null, 2)}`);
        /*const {responseData} = */
        yield call(() => (process.env.NODE_ENV === 'production') ?
                         axios.post(endpointURIs.detail, body) :
                         axios.get(endpointURIs.detail, {params: body})
        );
        // Since we are only mocking the requests right now, we will assign an id to the request body and return it as the server data.
        // The new id will be the generated code (without dots) as Integer.
        const augmentedBody = {...body, id: parseInt(body.code.replace(/\./g, ''))};
        yield put({type: successAction(actions.ADD_NODE), payload: {parentType, parentId, name, data: augmentedBody}});
    } catch (error) {
        yield put({type: failureAction(actions.ADD_NODE), payload: {error, parentType, parentId, name}});
    }
};

const removeNodeSaga = function* (action) {
    const {
              payload: {
                  type,
                  id
              }
          } = action;

    try {
        if (!type) {
            throw new Error(`Error trying to remove node of unknown type: ${type}, id: ${id}.`);
        }

        const params = {id};
        // The HTTP Verb should be DELETE, but since React dev server does not support that verb, we will mock it in non-production envs.
        /*const {data: responseData} = */
        yield call(() => axios[process.env.NODE_ENV === 'production' ? 'delete' : 'get'](endpointURIs.detail, {params}));
        yield put({type: successAction(actions.REMOVE_NODE), payload: {type, id}});
    } catch (error) {
        yield put({type: failureAction(actions.REMOVE_NODE), payload: {error, id}});
    }
};

export default [
    takeEvery(actions.LOAD_NODE_LIST, loadNodeListSaga),
    takeEvery(actions.ADD_NODE, addNodeSaga),
    takeEvery(actions.REMOVE_NODE, removeNodeSaga),
    ...detailSagas
];