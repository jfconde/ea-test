/**
 * Utility functions reused in the project.
 */
export const pickBy = (o, predicate) => {
    const res = {};
    Object.keys(o).filter(k => predicate(k, o[k])).forEach(k => res[k] = o[k]);
    return res;
};

/**
 * Handle a hash/map of <actionType: string, (state: object, action: object, payload: object?) => newState>.
 * @param {Object} actionsHash Hash/map of handled actions to their reducer functions (<actionType: string, (state: object, action: object, payload: object?) => newState>).
 * @param {Object} initialState Initial state for the reducer.
 * @returns {function(*=, *=): new reducer state}
 */
export const handleActions = (actionsHash = {}, initialState = {}) => (state = initialState, action) =>
    Object.keys(actionsHash)
        .filter(k => Array.isArray(k) ? k.includes(action.type) : k === action.type)
        .reduce((curState, key) => actionsHash[key](curState, action, (action && action.payload) || {}) || console.warn(`handleActions: reducer returned null-sy value for ${action.type}.`) === -1 || curState, state);

/**
 * Combine a reducer with child reducers, making the child reducers' data accessible as a key in the main reducer's state.
 *
 * Example: reducer1, reducer2, reducer3. Each of them has a state of {a: 0}.
 * withChildReducers(reducer1, {reducer2, reducer3}) would generate a reducer state such as:
 * {
 *     a: 0,
 *     reducer2: {a: 0},
 *     reducer3: {a: 0}
 * }
 *
 * @param {function} reducer The main reducer
 * @param {Object} childReducerHash Hash/map of state key to the child reducer (<reducerKey: string, reducer: function>).
 * @returns {function(*=, *=): {[p: string]: *}} The new reducer with its children combined.
 */
export const withChildReducers = (reducer, childReducerHash) => (state, action = {}) => {
    const childKeys = Object.keys(childReducerHash);
    return childKeys.reduce((curState, key) => ({
        ...curState,
        [key]: childReducerHash[key](state ? state[key] : undefined, action || {})
    }), reducer(state ? pickBy(state, k => !childKeys.includes(k)) : undefined, action));
};

// Utility function to append _SUCCESS/_FAILED to susceptible actions.
export const successAction = (actionType) => `${actionType}_SUCCESS`;
export const failureAction = (actionType) => `${actionType}_FAILED`;

export const uniqueArray = (array) => [...new Set(array)];