import React from 'react';
import LocCMSViewport from './locCMS/Viewport';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

const Viewport = ({routes = {}}) => (
    <div className="app-viewport">
        <Router>
            <Switch>
                <Route path={routes.locCMS.root} component={LocCMSViewport}/>
                <Route path="*" render={() => <Redirect to={routes.defaultRoute}/>}/>
            </Switch>
        </Router>
    </div>
);

export default Viewport;