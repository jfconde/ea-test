const root = process.env.PUBLIC_PATH || '';
const locCMSRoutePart = 'locCMS';

let routes = {
    locCMS: {
        root  : `${root}/${locCMSRoutePart}`,
        detail: `${root}/${locCMSRoutePart}/:id`
    }
};
routes.defaultRoute = routes.locCMS.root;

export default routes;