import {combineReducers} from 'redux';
import locCMS from './app/locCMS/reducer';

export default combineReducers({
    locCMS
});